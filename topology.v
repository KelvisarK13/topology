(* How do to topology in Coq if you are secretly an HOL fan.
   We will not use type classes or canonical structures because they
   count as "advanced" technology. But we will use notations.
 *)

(* We think of subsets as propositional functions.
   Thus, if [A] is a type [x : A] and [U] is a subset of [A],
   [U x] means "[x] is an element of [U]".
*)
Definition P (A : Type) := A -> Prop.

(* We will need the axiom of function extensionality, which Coq does not have. So we
   assume it here. The axiom says: if f and g give the same values, then they are equal. *)
Axiom funext :
  forall (X : Type) (P : X -> Type) (f g : forall x, P x),
    (forall x, f x = g x) -> f = g.

(* We also need the axiom of extensionalty for subsets, which says that two subsets are
   equal if they have the same elements. *)
Axiom subext :
  forall (X : Type) (U V : P X),
    (forall x, U x <-> V x) -> U = V.

(* A subset is in general defined as [fun x : A => ...]. We introduce
   a more familiar subset notation. NB: We are overriding Coq's standard
   notation for dependent sums. Luckily, we are not going to use dependent
   sums, so this is not a problem. *)
(* Notation "{ x : A | P }" := (fun x : A => P). *)

Definition singleton {A : Type} (x : A) := (fun y => x = y).

(* Definition and notation for subset relation. *)
Definition subset {A : Type} (u v : P A) :=
  forall x : A, u x -> v x.

Notation "u <= v" := (subset u v).

(* Disjointness. *)
Definition disjoint {A : Type} (u v : P A) :=
  forall x, ~ (u x /\ v x).

(* We are going to write a lot of statements of the form
   [forall x : A, U x -> P] and we'd like to write
   them in a less verbose manner. We introduce a notation
   that lets us write [all x : U, P] -- Coq will figure
   out what [A] is. *)
(* Notation "'all' x : U , P" := (forall x, U x -> P) (at level 20, x at level 99, only parsing). *)

(* Similarly for existence. *)
(* Notation "'some' x : U , P" := (exists x, U x /\ P) (at level 20, x at level 99, only parsing). *)

(* Arbitrary unions and binary intersections *)

Definition union {A : Type} (S : P (P A)) (x : A) :=
  exists (u : P A), S u /\ u x.

Definition inter {A : Type} (u v : P A) (x : A) :=
  u x /\ v x.

(* Binary union. *)
Definition union2 {A : Type} (u v : P A) (x : A) :=
  u x \/ v x.

(* Infix notation for intersections. *)
Notation "u * v" := (inter u v).

(* The empty and the full set. *)

Definition empty {A : Type} (x : A) := False.

Definition full {A : Type} (x : A) := True.

(* A topology in a type [A] is a structure which consists
   of a family of subsets (called the opens), satisfying
   the usual axioms. In Coq this amounts to the following
   definition. The mysterious [:>] means "automatically
   coerce from the topology structure to its opens". This
   is useful as it lets us write [U : T] instead of
   [U : opens T].
*)

Structure topology (A : Type) := {
  open :> P A -> Prop ;
  empty_open : open empty ;
  full_open : open full ;
  inter_open : forall u, open u -> forall v, open v -> open (u * v) ;
  union_open : forall S, S <= open -> open (union S)
}.

(* The discrete topology on a type. *)
Definition discrete (A : Type) : topology A.
Proof.
  exists full ; firstorder.
Defined.

(* The definition of a T_1 space. *)
Definition T1 {A : Type} (T : topology A) :=
  forall x y : A,
    x <> y ->
      exists u, T u /\ (u x /\ ~ (u y)).

(* The definition of Hausdorff space. *)
Definition hausdorff {A : Type} (T : topology A) :=
  forall x y : A,
  x <> y ->
    exists u v, T u /\ T v /\ u x /\ v y /\ disjoint u v.

(* A discrete space is Hausdorff. *)
Lemma discrete_hausdorff {A : Type} : hausdorff (discrete A).
Proof.
  intros x y N.
  exists (singleton x).
  exists (singleton y).
  repeat split.
  intros z [H1 H2].
  absurd (x = y) ; auto.
  transitivity z ; auto.
Qed.

(* Every Hausdorff space is T1. *)
Lemma hausdorff_is_T1 {A : Type} (T : topology A) :
  hausdorff T -> T1 T.
Proof.
  intros H x y N.
  unfold hausdorff in H.
  destruct (H x y N) as [u [v [u_open [v_open [x_in_u [y_in_v disj_u_v]]]]]].
  exists u ; repeat split ; auto.
  intro.
  absurd (u y /\ v y) ; auto.
Qed.

(* Indiscrete topology. A classical mathematician will be tempted to use
   disjunction: a set is open if it is either empty or the whole set. But
   that relies on excluded middle and generally causes trouble. Here is
   a better definition: a set is open iff as soon as it contains an element,
   it is the whole set. *)
Definition indiscrete (A : Type) : topology A.
Proof.
  simple refine {| open := (fun u : P A => forall x : A, u x -> (forall y : A, u y)) |} ;
    firstorder.
Defined.

(* Let us prove that the indiscrete topology is the least one. *)
Lemma indiscrete_least (A : Type) (T : topology A) :
  indiscrete A <= T.
Proof.
  intros u H.
  (* Idea: if u is in the indiscrete topology then it is the union of all
     T-opens v which it meets. *)
  assert (G : (u = union (fun v : P A => T v /\ exists x, v x /\ u x ))).
  - apply subext.
    intro x.
    split.
    + intros ?. exists full ; firstorder using full_open.
    + intros [v [[? [y [? ?]]] ?]] ; now apply (H y).
  - rewrite G.
    apply union_open.
    firstorder.
Qed.

(* Particular point topology, see
   http://en.wikipedia.org/wiki/Particular_point_topology, but of course
   without unecessary excluded middle.
*)
Definition particular {A : Type} (x : A) : topology A.
Proof.
  exists (fun u : P A => (exists y, u y) -> u x) ; firstorder.
Qed.

(* The topology generated by a family B of subsets that are
   closed under finite intersections. *)
Definition generated_topology {A : Type} (B : P (P A)) :
  B full ->
  (forall u v, B u -> B v -> B (u * v)) ->
  topology A.
Proof.
  intros H G.
  simple refine {| open := (fun u : P A => forall x, u x <-> exists v, B v /\ (v x /\ v <= u)) |}.
  - firstorder.
  - firstorder.
  - intros u Hu v Hv x.
    split.
    + intros [Gu Gv].
      destruct (proj1 (Hu x) Gu) as [u' [? [? ?]]].
      destruct (proj1 (Hv x) Gv) as [v' [? [? ?]]].
      exists (u' * v') ; firstorder.
    + intros [w [? [? ?]]].
      split ; now apply H2.
  - intros S K x.
    split.
    + intros [u [H1 H2]].
      destruct (K u H1 x) as [L1 _].
      destruct (L1 H2) as [v ?].
      exists v ; firstorder.
    + firstorder.
Defined.

Require Import List.

(* The intersection of a finite list of subsets. *)
Definition inters {A : Type} (us : list (P A)) : P A :=
  (fun x : A => Forall (fun u => u x) us).

Definition unions {A : Type} (us : list (P A)) : P A :=
  (fun x : A => Exists (fun u => u x) us).

Definition union_of_list_images {A B} (f : A -> B) (us : list (P A)) : P B :=
 (fun y : B => exists x, unions us x /\ f x  = y).

(* The closure of a family of sets by finite intersections. *)
Definition inter_close {A : Type} (S : P (P A)) :=
  (fun v : P A => exists us, Forall S us /\ (forall x, v x <-> inters us x)).

Lemma Forall_app {A : Type} (l1 l2 : list A) (P : A -> Prop) :
  Forall P l1 -> Forall P l2 -> Forall P (l1 ++ l2).
Proof.
  induction l1 ; simpl ; auto.
  intros H G.
  constructor.
  - apply (Forall_inv H).
  - apply IHl1 ; auto.
    inversion H ; assumption.
Qed.

Lemma Forall_app1 {A : Type} (l1 l2 : list A) (P : A -> Prop) :
  Forall P (l1 ++ l2) -> Forall P l1.
Proof.
  induction l1 ; simpl ; auto.
  intro H.
  inversion H ; auto.
Qed.

Lemma Forall_app2 {A : Type} (l1 l2 : list A) (P : A -> Prop) :
  Forall P (l1 ++ l2) -> Forall P l2.
Proof.
  induction l1 ; simpl ; auto.
  intro H.
  inversion H ; auto.
Qed.

(* The topology generated by a subgenerated_topology S. *)
Definition subgenerated_topology {A : Type} (S : P (P A)) : topology A.
Proof.
  apply (generated_topology (inter_close S)).
  - exists nil ; firstorder using Forall_nil.
  - intros u v [us [Hu Gu]] [vs [Hv Gv]].
    exists (us ++ vs).
    split ; [ (now apply Forall_app) | idtac ].
    split.
    + intros [? ?].
      apply Forall_app ; firstorder.
    + intro K ; split.
      * apply Gu.
        apply (Forall_app1 _ _ _ K).
      * apply Gv.
        apply (Forall_app2 _ _ _ K).
Defined.

(* A subbasic set is open. *)
Lemma subgenerated_topology_open {A : Type} (S : P (P A)) (u : P A) :
  S u -> (subgenerated_topology S) u.
Proof.
  intros H x.
  split.
  - intro G.
    exists u ; split ; [ idtac | firstorder ].
    exists (u :: nil).
    split ; [now constructor | idtac].
    intro y ; split.
    + intro ; now constructor.
    + intro K.
      inversion K ; auto.
  - firstorder.
Qed.

(* The cofinite topology on A. *)
Definition cofinite (A : Type) : topology A :=
  subgenerated_topology (fun u : P A => exists x, forall y, (u y <-> y <> x)).

(* The cofinite topology is T1. *)
Lemma cofinite_T1 (A : Type) : T1 (cofinite A).
Proof.
  intros x y N.
  exists (fun z : A => z <> y).
  split ; auto.
  apply subgenerated_topology_open.
  exists y ; firstorder.
Qed.

Definition id1 (X : Type) (x : X) := x.

Arguments id1 {_} _.

Definition cow := (fun y => @id1 bool y).

Definition id {X : Type} (x : X) := x.

Definition compose {A B C} (g : B -> C) (f : A -> B) : A -> C :=
 fun x => g (f x).

(* Inverse image. *)
Definition inv {A B} (f : A -> B) (v : P B) : P A :=
  fun x => v (f x).
  (* actually equal to [compose v f]. *)

(* Inverse image for family of sets. *)
Definition inv_F {A B} (f : A -> B) (F : P (P B)) : P (P A) :=
  fun X => exists v, F v /\ X = inv f v.

(* Direct image. *)
Definition image {A B} (f : A -> B) (u : P A) : P B :=
  fun y => exists x, u x /\ f x = y.

(*Direct image of list elements. *)
Fixpoint images {A B} (f : A -> B) (us : list (P A)) : list (P B) :=
match us with
| nil => nil
| cons u v => cons (image f u) (images f v)
end.

Definition is_continuous {X Y} (S : topology X) (T : topology Y) (f : X -> Y) :=
  forall v, T v -> S (inv f v).

Lemma id_is_continuous X (T : topology X) : is_continuous T T (fun x => x).
Proof.
  firstorder.
Qed.

Lemma composition_inverse_image X Y Z (u : P Z)
      (f : X -> Y) (g : Y -> Z) :
      inv (compose g f) u = inv f (inv g u).
Proof.
  apply funext.
  intro x.
  reflexivity.
Qed.

Lemma composition_is_continuous X Y Z
      (R : topology X) (S : topology Y) (T : topology Z)
      (f : X -> Y) (g : Y -> Z) :
  is_continuous R S f -> is_continuous S T g -> is_continuous R T (compose g f).
Proof.
  intros cont_f cont_g.
  intros U open_U.
  rewrite composition_inverse_image.
  now apply cont_f, cont_g.
Qed.

Lemma continuous_left_antimonotone X Y (R S : topology X)  (T : topology Y)
      (f : X -> Y) :
  R <= S -> is_continuous R T f -> is_continuous S T f.
Proof.
  firstorder.
Qed.

Lemma inverse_image_of_union X Y
      (f : X -> Y)
      (D : P (P Y)) :
  inv f (union D) = union (inv_F f D).
Proof.
  apply subext.
  intro x.
  split.
  - intros [u [u_in_D fx_in_u]].
    exists (inv f u).
    split.
    + exists u.
      split.
      * assumption.
      * reflexivity.
    + assumption.
  - intros [U [[V [V_in_D U_is_invfV]] x_in_U]].
    exists V.
    split.
    + assumption.
    + rewrite U_is_invfV in x_in_U.
      exact x_in_U.
Qed.

(* A set which is open in the topology generated by a base is a union of basic open sets. *)
Lemma generated_topology_open_iff_union_of_basic X
      (B : P (P X)) (p1 : B full) (p2 : (forall u v, B u -> B v -> B (u * v))) (U : P X) :
  (generated_topology B p1 p2) U -> exists D : P (P X), (D <= B) /\ U = union D.
Proof.
  intro open_U.
  exists (fun V : P X => B V /\ V <= U).
  split.
  - now intros V [? ?].
  - apply subext.
    intro x.
    split.
    + intro x_in_U.
      destruct (proj1 (open_U x) x_in_U) as [V [V_in_B [x_in_V V_sub_U]]].
      now exists V.
    + intros [V [[V_in_B V_sub_U] x_in_V]].
      now apply V_sub_U.
Qed.

Lemma continuous_on_a_generated_topology X Y
      (R : topology X)
      (B : P (P Y)) (p1 : B full) (p2 : (forall u v, B u -> B v -> B (u * v)))
      (f : X -> Y) :
  (forall (u : P Y ), (B u) -> R (inv f u)) -> (is_continuous R (generated_topology B p1 p2) f).
Proof.
  intros H V V_in_B.
  destruct (generated_topology_open_iff_union_of_basic Y B p1 p2 V V_in_B) as [D [D_sub_B E]].
  rewrite E.
  rewrite inverse_image_of_union.
  apply union_open.
  intros U [V' [V'_in_D U_is_invV']].
  rewrite U_is_invV'.
  apply H.
  apply D_sub_B.
  assumption.
Qed.

Definition compact_subspace {A : Type} (T : topology A) (K : P A) :=
  forall (D : P (P A)),
    D <= T -> K <= union D -> exists us : list (P A), Forall D us /\ K <= unions us.

Lemma compact_empty {A : Type} (T : topology A) : compact_subspace T empty.
Proof.
  unfold compact_subspace.
  intros D H G.
  exists nil. split.
  - auto.
  - unfold subset ; firstorder.
Qed.

Lemma union2_subsets {X : Type} (A B C D : P X) : (A <= C /\ B <= D) -> (union2 A B) <= (union2 C D).
Proof.
  intros H.
  destruct H.
  unfold union2.
  firstorder.
Qed.

Lemma union2_unions {A : Type} (l1 l2 : list (P A)) : union2 (unions l1) (unions l2) = unions (l1 ++ l2).
Proof.
  (*unfold union2. unfold unions.*)
  apply subext.
  intros x.
  split.
  intros union2.
  firstorder. elim H. firstorder. Print Exists. apply Exists_cons_hd. assumption.
  intros x0 l H1 H2. unfold unions. Print Exists. apply Exists_cons_tl. admit.
  unfold unions. Print Exists. induction l1. compute. apply H. admit.
  intro H. elim H. firstorder. unfold union2. destruct H. left. admit.
Admitted.

Lemma compact_singleton {A : Type} (T : topology A) (x : A) :
  compact_subspace T (singleton x).
Proof.
  unfold compact_subspace. unfold union. unfold unions. unfold singleton. unfold subset.
  intros D D_subset_T x_in_D. 
  assert(xinD2 : union D x). firstorder.
  assert (xinU : exists U, D U /\ U x). apply xinD2.
  assert (xunderU : exists U, D U /\ singleton x <= U).
  - unfold singleton.
    unfold subset.
    firstorder.
    exists x1.
    firstorder.
    replace x2 with x.
    firstorder.
  - destruct xunderU as [U xunderU1]. destruct xunderU1 as [xunderU11 xunderU12].
  exists (U :: nil). split.
  Print Forall. apply Forall_cons.
    + assumption.
    + apply Forall_nil.
  + intros x0 xeqx0. Print Exists. apply Exists_cons_hd. replace x0 with x. firstorder.
Qed.

Lemma compact_union {A : Type} (T : topology A) (K1 K2 : P A) :
  compact_subspace T K1 ->
  compact_subspace T K2 ->
  compact_subspace T (union2 K1 K2).
Proof.
(*unfold compact_subspace. unfold union2. *)
intros K1_compact K2_compact.
(*unfold compact_subspace. *)
intros D D_in_T Union.
assert (H : exists us : list (P A), Forall D us /\ K1 <= unions us). apply K1_compact.
apply D_in_T. firstorder.
assert (G : exists us : list (P A), Forall D us /\ K2 <= unions us). apply K2_compact.
apply D_in_T. firstorder.
destruct H as [s1 H1].
destruct G as [s2 H2].
exists (s1 ++ s2). destruct H1. destruct H2. split. apply Forall_app. assumption. assumption.
unfold subset. intros x x_in_union_K. replace(unions (s1++s2)) with (union2 (unions s1) (unions s2)). firstorder.
apply union2_unions.
Qed.

Lemma Forall_begin {A : Type} (D : P (P A)) (l : list (P A)) (a : P A) :
  Forall D (a :: l) <-> D a /\ Forall D l.
Proof.
  split.
  intros H.
  split.
  Print Forall. apply (Forall_inv H). apply Forall_app2 with (l1 := (a :: nil)). apply H.
  intros F.
  destruct F. apply Forall_cons. assumption. assumption.
Qed.


Lemma image_of_unions {A B : Type} (f : A -> B)(l : list (P A)):
  image f (unions l) = unions (images f l).
Proof.
  apply subext. intros x.
  split.
  + unfold image. unfold unions. intros. unfold images. admit.
Admitted.

Lemma List_we_searched_for {A B: Type} (f : A -> B) (D : P (P B))(l : list (P A)):
  Forall (inv_F f D) l -> exists s : list (P B), Forall D s /\ forall a : (P A), In a l -> (exists d : (P B), (In d s) /\ (D d) /\ (a = inv f d)).
Proof.
  intros H. induction l. 
  + exists nil. intros. firstorder.
  + assert (F: Forall (inv_F f D) l). Print Forall. apply Forall_app2 with (l1 := a :: nil). apply H.
    assert (G : exists s : list (P B),
        Forall D s /\
        (forall a : P A,
         In a l ->
         exists d : P B, In d s /\ D d /\ a = inv f d)).
    apply IHl. assumption. destruct G.
    assert (G1 : exists d1 : (P B), D d1 /\ a = inv f d1). firstorder. Print Forall. apply (Forall_inv H). destruct G1.
    exists (x0 :: x). firstorder. exists x0. firstorder. replace a0 with a. assumption.
Qed.

Lemma compact_image {A B : Type} (S : topology A) (T : topology B)
      (f : A -> B) (K : P A) :
  is_continuous S T f ->
  compact_subspace S K ->
  compact_subspace T (image f K).
Proof.
  intros f_cont K_comp.
  unfold compact_subspace.
  intros D D_in_T fK_in_D.
  assert (H: (inv_F f D) <= S -> K <= union (inv_F f D)).
  intro openness.
  unfold subset.
  intros x x_in_K.
  replace (union (inv_F f D)) with (inv f (union D)). firstorder.
  apply inverse_image_of_union.
  assert (F: exists us : list (P A), Forall (inv_F f D) us /\ K <= unions us).
  apply K_comp.
  unfold subset.
  intros u u_in_inv_D. firstorder.
  replace u with (inv f x).
  apply f_cont. firstorder.
  apply H.
  unfold subset.
  intros u u_in_inv_D. firstorder.
  replace u with (inv f x).
  apply f_cont. firstorder.
  destruct F as [l ?].
  destruct H0.
  assert (Z : exists s : list (P B), Forall D s /\ forall a : (P A), In a l -> (exists d : (P B), (In d s) /\ (D d) /\ (a = inv f d))).
  apply List_we_searched_for. assumption.
  destruct Z.
  exists x. split.
  + firstorder.
  + assert (I : (image f (unions l)) <= unions x). admit.
Admitted.

Definition complement {A : Type} (S : P A) (x : A) := ~ S x.

Definition is_closed {A : Type} (T : topology A) (S : P A) :=
  T (complement S).

Lemma open_iff_union_of_opens {A : Type} (T : topology A) (u : P A) :
  (forall x, u x -> exists v, T v /\ v x /\ v <= u) -> T u.
Proof.
  intros H.
Admitted.

Lemma hausdorff_compact_closed {A : Type} (T : topology A) (K : P A) :
  hausdorff T ->
  compact_subspace T K ->
  is_closed T K.
Proof.
  intros H G.
  unfold is_closed.
  apply open_iff_union_of_opens.
  intros x x_in_complement_K.
  (* najprej definiramo pokritje *)
  pose (D := fun v : P A => exists y u, v y /\ K y /\ u x /\ disjoint u v).
  assert (D_opens : D <= T).
  { admit. }
  assert (D_covers : K <= union D).
  { admit. }
  destruct (G D D_opens D_covers) as [us [? ?]].
  admit.
Admitted.

